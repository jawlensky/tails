# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-08-24 19:56+0000\n"
"PO-Revision-Date: 2023-12-05 10:42+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 5.3.1 is out\"]]\n"
msgstr "[[!meta title=\"Tails 5.3.1 is out\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Tue, 02 Aug 2022 12:34:56 +0000\"]]\n"
msgstr "[[!meta date=\"Tue, 02 Aug 2022 12:34:56 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
msgid ""
"This release is an emergency release to fix a security vulnerability in the "
"*Linux* kernel."
msgstr ""
"Diese Version ist eine Notfallversion, um eine Sicherheitslücke im *Linux*-"
"Kernel zu beheben."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"changes\">Changes and updates</h1>\n"
msgstr "<h1 id=\"changes\">Änderungen und Aktualisierungen</h1>\n"

#. type: Bullet: '- '
msgid ""
"Update the *Linux* kernel to 5.10.127-2, which fixes [CVE-2022-34918]"
"(https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-34918), a "
"vulnerability that allows applications in Tails to gain administration "
"privileges."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  For example, if an attacker was able to exploit other unknown security\n"
"  vulnerabilities in *Tor Browser*, they might then use CVE-2022-34918 to take\n"
"  full control of your Tails and deanonymize you.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  This attack is very unlikely, but could be performed by a strong attacker,\n"
"  such as a government or a hacking firm. We are not aware of this attack being\n"
"  used in the wild.\n"
msgstr ""

#. type: Plain text
msgid ""
"- Update *Thunderbird* to [91.12.0](https://www.thunderbird.net/en-US/"
"thunderbird/91.12.0/releasenotes/)."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"fixes\">Fixed problems</h1>\n"
msgstr "<h1 id=\"fixes\">Behobene Probleme</h1>\n"

#. type: Plain text
msgid ""
"For more details, read our [[!tails_gitweb debian/changelog desc=\"changelog"
"\"]]."
msgstr ""
"Für weitere Details lesen Sie bitte unser [[!tails_gitweb debian/changelog "
"desc=\"changelog\"]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"issues\">Known issues</h1>\n"
msgstr "<h1 id=\"issues\">Bekannte Probleme</h1>\n"

#. type: Plain text
msgid "None specific to this release."
msgstr "Keine spezifischen Angaben zu dieser Version."

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr "Siehe die Liste der [[Altlasten|support/known_issues]]."

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"get\">Get Tails 5.3.1</h1>\n"
msgstr ""

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr ""
"So aktualisieren Sie Ihren Tails USB-Stick und behalten Ihren dauerhaften "
"Speicher"

#. type: Plain text
msgid "- Automatic upgrades are available from Tails 5.0 or later to 5.3.1."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"  You can [[reduce the size of the download|doc/upgrade#reduce]] of future\n"
"  automatic upgrades by doing a manual upgrade to the latest version.\n"
msgstr ""
"  Sie können [[die Größe des Downloads|doc/upgrade#reduce]] von zukünftigen\n"
"  automatischen Upgrades reduzieren, indem Sie ein manuelles Upgrade auf die "
"neueste Version durchführen.\n"

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade/#manual]]."
msgstr ""
"Wenn Sie kein automatisches Upgrade durchführen können oder wenn Tails nach "
"einem automatischen Upgrade nicht startet, versuchen Sie bitte ein [["
"manuelles Upgrade|doc/upgrade/#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "So installieren Sie Tails auf einem neuen USB-Stick"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Folgen Sie unserer Installationsanleitung:"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Installieren von Windows|install/windows]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Installieren von macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Installieren von Linux|install/linux]]"

#. type: Bullet: '  - '
msgid ""
"[[Install from Debian or Ubuntu using the command line and GnuPG|install/"
"expert]]"
msgstr ""
"[[Installation von Debian oder Ubuntu über die Kommandozeile und GnuPG|"
"install/expert]]"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"caution\"><p>The Persistent Storage on the USB stick will be lost if\n"
"you install instead of upgrading.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>Der persistente Speicher auf dem USB-Stick geht "
"verloren, \n"
"wenn Sie die Installation statt des Upgrades durchführen.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Nur zum Herunterladen"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 5.3.1 directly:"
msgstr ""

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Für USB-Sticks (USB-Image)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Für DVDs und virtuelle Maschinen (ISO-Image)|install/download-iso]]"

#. type: Plain text
#, no-wrap
msgid "<h1 id=\"next\">What's coming up?</h1>\n"
msgstr ""

#. type: Plain text
msgid "Tails 5.4 is [[scheduled|contribute/calendar]] for August 23."
msgstr ""
