# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-03-15 18:33+0000\n"
"PO-Revision-Date: 2023-11-15 21:14+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Accessing resources on the local network\"]]\n"
msgstr "[[!meta title=\"Accés a recursos a la xarxa local\"]]\n"

#. type: Plain text
msgid ""
"The term \"local network\" here refers to the set of computers and devices "
"that can be reached directly from your computer without going through the "
"Internet. For example, your home router, your network printer, or the "
"intranet of your company are most likely on your local network, also called "
"LAN for Local Area Network. In technical terms, this refers to the set of IP "
"addresses defined in [RFC1918](https://tools.ietf.org/html/rfc1918)."
msgstr ""
"El terme \"xarxa local\" aquí fa referència al conjunt d'ordinadors i "
"dispositius als quals es pot accedir directament des del vostre ordinador "
"sense passar per Internet. Per exemple, el vostre encaminador domèstic "
"(router), la vostra impressora de xarxa o la intranet de la vostra empresa "
"és molt probable que estiguin a la vostra xarxa local, també anomenada LAN o "
"Local Area Network, en anglès. En termes tècnics, es refereix al conjunt "
"d'adreces IP definides a [RFC1918](https://tools.ietf.org/html/rfc1918)."

#. type: Title =
#, no-wrap
msgid "Security considerations"
msgstr "Consideracions de seguretat"

#. type: Plain text
msgid ""
"Accessing resources on the local network can be useful in the context of "
"Tails, for example to exchange documents with someone on the same local "
"network without going through the Internet."
msgstr ""
"L'accés als recursos de la xarxa local pot ser útil en el context de Tails, "
"per exemple per intercanviar documents amb algú de la mateixa xarxa local "
"sense passar per Internet."

#. type: Plain text
#, no-wrap
msgid ""
"But an application that can connect to both resources on the\n"
"Internet (going through Tor) and resources on the local network (without going\n"
"through Tor) can break your anonymity. For example, if a website that\n"
"you visit anonymously using <span class=\"application\">Tor Browser</span> could also connect to other\n"
"web pages that are specific to your local network, then this information\n"
"could reveal where you are. This is why <span class=\"application\">Tor Browser</span> is prevented from\n"
"accessing the local network in Tails.\n"
msgstr ""
"Però una aplicació que es pot connectar tant a recursos d'Internet\n"
"(passant per Tor) com a recursos de la xarxa local (sense passar per Tor)\n"
"podria trencar el vostre anonimat. Per exemple, si un lloc web\n"
"que visiteu de manera anònima utilitzant el <span class=\"application\""
">Navegador Tor</span> també\n"
"pogués connectar-se a altres pàgines web específiques de la vostra xarxa "
"local,\n"
"llavors aquesta informació podria revelar on sou. És per això que el <span "
"class=\"application\">Navegador Tor</span>\n"
"no pot accedir a la xarxa local a Tails.\n"

#. type: Plain text
msgid ""
"This page describes some of the security measures built in Tails to protect "
"from such attacks and explains how to access some types of resources on the "
"local network."
msgstr ""
"Aquesta pàgina descriu algunes de les mesures de seguretat integrades a "
"Tails per protegir-vos d'aquests atacs i s'explica com accedir a alguns "
"tipus de recursos a la xarxa local."

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>Connections made to the local network are not anonymous and do not go\n"
"through Tor.</p>\n"
msgstr ""
"<p>Les connexions fetes a la xarxa local no són anònimes i no passen\n"
"a través de Tor.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"browser\"></a>\n"
msgstr "<a id=\"browser\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Browsing web pages on the local network"
msgstr "Navegació de pàgines web a la xarxa local"

#. type: Plain text
#, no-wrap
msgid ""
"It is impossible to access web pages on the local network using <span class=\"application\">Tor\n"
"Browser</span>. This prevents websites on the Internet from deducing your\n"
"location from the content of other web pages that might be specific to your local\n"
"network.\n"
msgstr ""
"És impossible accedir a pàgines web a la xarxa local mitjançant el <span "
"class=\"application\">Navegador\n"
"Tor</span>. Això evita que els llocs web a Internet dedueixin la vostra\n"
"ubicació a partir del contingut d'altres pàgines web que poden ser "
"específiques de la vostra xarxa\n"
"local.\n"

#. type: Plain text
#, no-wrap
msgid ""
"To access web pages on the local network, use the [[<span class=\"application\">Unsafe\n"
"Browser</span>|anonymous_internet/unsafe_browser]] instead.\n"
msgstr ""
"Per accedir a pàgines web a la xarxa local, utilitzeu el [[<span class="
"\"application\">Navegador\n"
"Insegur</span>|anonymous_internet/unsafe_browser]].\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"http\"></a>\n"
msgstr "<a id=\"http\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Downloading files from web pages on the local network"
msgstr "Baixar fitxers de pàgines web a la xarxa local"

#. type: Plain text
#, no-wrap
msgid ""
"To download files from web pages on the local network, you can use the\n"
"`curl` command instead. For example, to download a document available on\n"
"the local network at <span class=\"filename\">http://192.168.1.40/document.pdf</span>\n"
"execute the following command:\n"
msgstr ""
"Per baixar fitxers de pàgines web a la xarxa local, podeu utilitzar el\n"
"l'ordre `curl`. Per exemple, per baixar un document disponible a\n"
"la xarxa local a <span class=\"filename\">http://192.168.1.40/document."
"pdf</span>\n"
"executeu l'ordre següent:\n"

#. type: Plain text
#, no-wrap
msgid "    curl http://192.168.1.40/document.pdf\n"
msgstr "    curl http://192.168.1.40/document.pdf\n"

#, no-wrap
#~ msgid "[[!inline pages=\"doc/anonymous_internet/unsafe_browser/chroot.inline\" raw=\"yes\" sort=\"age\"]]\n"
#~ msgstr "[[!inline pages=\"doc/anonymous_internet/unsafe_browser/chroot.inline.ca\" raw=\"yes\" sort=\"age\"]]\n"

#~ msgid "Downloading files from an FTP server on the local network\n"
#~ msgstr "Dateien von einem FTP-Server im lokalen Netzwerk herunterladen\n"

#, fuzzy
#~| msgid "    curl http://192.168.1.40/document.pdf\n"
#~ msgid "       ftp://192.168.1.25/\n"
#~ msgstr "    curl http://192.168.1.40/document.pdf\n"

#~ msgid "<a id=\"ftp\"></a>\n"
#~ msgstr "<a id=\"ftp\"></a>\n"
