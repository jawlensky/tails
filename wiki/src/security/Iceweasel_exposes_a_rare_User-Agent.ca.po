# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2018-10-01 16:15+0000\n"
"PO-Revision-Date: 2023-12-05 23:42+0000\n"
"Last-Translator: victor dargallo <victordargallo@disroot.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Fri, 03 Sep 2010 01:15:14 +0000\"]]\n"
msgstr "[[!meta date=\"Fri, 03 Sep 2010 01:15:14 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Iceweasel exposes a rare User-Agent\"]]\n"
msgstr "[[!meta title=\"Iceweasel exposa un agent d'usuari estrany\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid ""
"A Torbutton bug ([[!debbug 595375]]) makes Iceweasel expose a recognizable "
"User-Agent when the \"Spoof US English Browser\" setting is disabled, which "
"is the case in T(A)ILS 0.5."
msgstr ""
"Un error de Torbutton ([[!debbug 595375]]) fa que Iceweasel exposi un agent "
"d'usuari reconeixible quan la configuració \"Spoof US English Browser\" està "
"desactivada, com és el cas de T(A)ILS 0.5."

#. type: Title #
#, no-wrap
msgid "Impact"
msgstr "Impacte"

#. type: Plain text
msgid ""
"System administrators, webmasters and anyone able to read the logs of a "
"website are able to single out, amongst the visitors, the ones that are "
"using an affected Torbutton extension *and* have explicitly disabled the "
"\"Spoof US English Browser\" setting."
msgstr ""
"Els administradors del sistema, els administradors web i qualsevol persona "
"capaç de llegir els registres d'un lloc web poden identificar, entre els "
"visitants, els que utilitzen una extensió Torbutton afectada *i* han "
"desactivat explícitament la configuració \"Spoof US English Browser\"."

#. type: Plain text
msgid ""
"While T(A)ILS users are obviously not the only ones in this case, such a bug "
"eases fingerprinting."
msgstr ""
"Tot i que els usuaris de T(A)ILS, òbviament, no són els únics en aquest cas, "
"aquest error facilita la presa d'empremtes digitals."

#. type: Plain text
msgid ""
"The client IP address recorded in the webserver logs for such a connection "
"is the one of the Tor exit node used by the T(A)ILS user at this time."
msgstr ""
"L'adreça IP del client registrada als registres del servidor web per a "
"aquesta connexió és la del node de sortida Tor que utilitza l'usuari T(A)ILS "
"en aquest moment."

#. type: Title #
#, no-wrap
msgid "Solution"
msgstr "Solució"

#. type: Plain text
msgid "Upgrade to T(A)ILS 0.6."
msgstr "Actualitzeu a T(A)ILS 0.6."

#. type: Title #
#, no-wrap
msgid "Mitigation on T(A)ILS 0.5"
msgstr "Mitigació a T(A)ILS 0.5"

#. type: Plain text
msgid ""
"The following steps need to be done immediately after boot, **before** "
"running Iceweasel."
msgstr ""
"Els passos següents s'han de fer immediatament després de l'arrencada, "
"**abans** d'iniciar Iceweasel."

#. type: Plain text
msgid "Run the following command in a terminal:"
msgstr "Executeu l'ordre següent en un terminal:"

#. type: Plain text
#, no-wrap
msgid "\tgksudo gedit /etc/iceweasel/profile/user.js\n"
msgstr "\tgksudo gedit /etc/iceweasel/profile/user.js\n"

#. type: Plain text
msgid "... this opens a text editor. Delete the line that says:"
msgstr "... això obre un editor de text. Suprimiu la línia que diu:"

#. type: Plain text
#, no-wrap
msgid "\tuser_pref(\"extensions.torbutton.spoof_english\", false);\n"
msgstr "\tuser_pref(\"extensions.torbutton.spoof_english\", false);\n"

#. type: Plain text
msgid "... then save and quit. You can now run Iceweasel."
msgstr "... després deseu i sortiu. Ara podeu iniciar Iceweasel."

#. type: Plain text
msgid ""
"Beware! Changing this setting in the Torbutton preferences window is **not** "
"effective."
msgstr ""
"Compte! Canviar aquesta configuració a la finestra de preferències de "
"Torbutton **no** és efectiu."

#. type: Title #
#, no-wrap
msgid "Affected versions"
msgstr "Versions afectades"

#. type: Plain text
msgid "Torbutton 1.2.5, included in T(A)ILS 0.5"
msgstr "Torbutton 1.2.5, inclòs a T(A)ILS 0.5"
