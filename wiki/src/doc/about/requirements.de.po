# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2023-11-10 17:08+0000\n"
"PO-Revision-Date: 2023-11-15 09:13+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: Tails translators <tails-l10n@boum.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"System requirements and recommended hardware\"]]\n"
msgstr "[[!meta title=\"Systemanforderungen und empfohlene Hardware\"]]\n"

#. type: Title =
#, no-wrap
msgid "Summary"
msgstr "Zusammenfassung"

#. type: Bullet: '- '
msgid "Tails works on most PC computers that are less than 10 years old."
msgstr "Tails läuft auf den meisten Rechnern die nicht älter als 10 Jahre sind."

#. type: Bullet: '  - '
msgid "Tails works on some older Mac computers with an Intel processor."
msgstr ""
"Tails läuft auf den meisten Macs mit Intel-Chips die nicht älter als 10 "
"Jahre sind."

#. type: Bullet: '- '
msgid ""
"Tails does not work on newer Mac computers with an Apple processor (M1 or "
"M2)."
msgstr ""
"Tails läuft nicht auf neueren Mac-Computern mit einem ARM-Apple-Prozessor ("
"M1 , M2 oder M3)."

#. type: Bullet: '- '
msgid "Tails might not work on:"
msgstr "Tails läuft möglicherweise nicht auf:"

#. type: Bullet: '  * '
msgid "Some older computers, for example, if they don't have enough RAM."
msgstr ""
"Einige ältere Computer haben zum Beispiel nicht genügend Arbeitsspeicher."

#. type: Bullet: '  * '
msgid ""
"Some newer computers, for example, if their [[graphics card is incompatible "
"with Linux|support/known_issues/graphics]]."
msgstr ""
"Einige neuere Computer, zum Beispiel, wenn ihre [[Grafikkarte nicht mit "
"Linux kompatibel ist|support/known_issues/graphics]]."

#. type: Bullet: '  * '
msgid ""
"\"Gaming\" graphics cards like Nvidia or AMD Radeon, which are often "
"incompatible."
msgstr ""
"\"Gaming\"-Grafikkarten wie Nvidia oder AMD Radeon, die oft nicht kompatibel "
"sind."

#. type: Plain text
#, no-wrap
msgid ""
"<p>See our [[list of known hardware compatibility\n"
"issues|support/known_issues]].</p>\n"
msgstr ""
"<p>Siehe unsere [[Liste bekannter Hardware-Kompatibilitätsprobleme|support/"
"known_issues]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Title =
#, no-wrap
msgid "Hardware requirements"
msgstr "Hardware Voraussetzungen"

#. type: Plain text
msgid "- A USB stick of 8 GB minimum or a recordable DVD."
msgstr "- Ein USB Stick mit mindestens 8 GB oder eine beschreibbare DVD."

#. type: Plain text
#, no-wrap
msgid "  All the data on this USB stick or DVD will be lost when installing Tails.\n"
msgstr "  Alle Daten auf dem USB Stick oder der DVD werden beim Installieren von Tails überschrieben.\n"

#. type: Plain text
msgid "- The ability to start from a USB stick or a DVD reader."
msgstr ""
"- Die Möglichkeit, von einem USB-Stick oder einem DVD-Lesegerät zu starten."

#. type: Bullet: '- '
msgid ""
"A 64-bit <span class=\"definition\">[[!wikipedia x86-64]]</span> compatible "
"processor:"
msgstr ""
"Ein 64-bit <span class=\"definition\">[[!wikipedia_de X64]]</span> "
"kompatibler Prozessor:"

#. type: Bullet: '  - '
msgid ""
"<span class=\"definition\">[[!wikipedia IBM_PC_compatible]]</span> but not "
"<span class=\"definition\">[[!wikipedia PowerPC]]</span> nor <span "
"class=\"definition\">[[!wikipedia ARM_architecture desc=\"ARM\"]]</span>."
msgstr ""
"<span class=\"definition\">[[!wikipedia_de IBM-PC-kompatibler_Computer]]</"
"span> aber weder <span class=\"definition\">[[!wikipedia_de PowerPC]]</span> "
"noch <span class=\"definition\">[[!wikipedia_de Arm-Architektur "
"desc=\"ARM\"]]</span>."

#. type: Bullet: '  - '
msgid ""
"Tails does not work on newer Mac computers with an [[!wikipedia "
"Apple_silicon#M_series desc=\"Apple processor\"]] (M1 or M2)."
msgstr ""
"Tails funktioniert nicht auf neueren Mac-Computern mit einem ARM <a href="
"\"https://de.wikipedia.org/wiki/Apple_Silicon#M-Serie\" title=\"Apple-"
"Prozessor\">Apple-Prozessor</a> (M1, M2 oder M3)."

#. type: Bullet: '  - '
msgid ""
"Tails does not work on 32-bit computers since [[Tails 3.0|news/"
"Tails_3.0_will_require_a_64-bit_processor]] (June 2017)."
msgstr ""
"Tails läuft nicht mehr auf 32-Bit Computern seit [[Tails 3.0|news/"
"Tails_3.0_will_require_a_64-bit_processor]] (Juni 2017)."

#. type: Bullet: '  - '
msgid "Tails does not work on phone or tablets."
msgstr "Tails läuft nicht auf Handys oder Tablets."

#. type: Plain text
msgid "- 2 GB of RAM to work smoothly."
msgstr "- 2GB RAM um flüssig zu laufen."

#. type: Plain text
#, no-wrap
msgid "  Tails can work with less than 2 GB RAM but might behave strangely or crash.\n"
msgstr "  Tails kann auch mit weniger als 2 GB RAM funktionieren, aber es verhält sich dann vielleicht unerwarteten oder stürzt ab.\n"

#. type: Title =
#, no-wrap
msgid "Recommended hardware"
msgstr "Empfohlene Hardware"

#. type: Plain text
msgid ""
"Laptop models evolve too rapidly for us to be able to provide an up-to-date "
"list of recommended hardware. Below are some guidelines if you, or your "
"organization, are considering acquiring a laptop dedicated to running Tails."
msgstr ""
"Laptop-Modelle entwickeln sich zu schnell weiter, als dass wir eine aktuelle "
"Liste der empfohlenen Hardware bereitstellen könnten. Im Folgenden finden "
"Sie einige Richtlinien, wenn Sie oder Ihre Organisation die Anschaffung "
"eines Laptops für den Betrieb von Tails in Erwägung ziehen."

#. type: Title ###
#, no-wrap
msgid "For PC"
msgstr "Für PC"

#. type: Bullet: '- '
msgid "Avoid \"gaming\" models with Nvidia or AMD Radeon graphics cards."
msgstr ""
"Vermeiden Sie \"Gaming\"-Modelle mit Nvidia- oder AMD-Radeon-Grafikkarten."

#. type: Bullet: '- '
msgid ""
"Consider buying a refurbished laptop from a high-end (professional) series.  "
"These are cheaper and will last longer than new but lower-quality laptops."
msgstr ""
"Erwägen Sie den Kauf eines generalüberholten Notebooks aus einer "
"hochwertigen (professionellen) Serie.  Diese sind billiger und halten länger "
"als neue, aber minderwertige Laptops."

#. type: Bullet: '  * '
msgid ""
"For example, the Lenovo ThinkPad series work well with Tails, including the "
"X250, X1 Carbon, T440, T480, and T490 models."
msgstr ""
"Die Lenovo ThinkPad-Serien funktionieren beispielsweise gut mit Tails, "
"darunter die Modelle X250, X1 Carbon, T440, T480 und T490."

#. type: Bullet: '  * '
msgid ""
"If you live in a part of the world where buying refurbished laptops is "
"uncommon, look on eBay and Amazon. Amazon offers a 90-day [Amazon Renewed "
"Guarantee](https://www.amazon.com/gp/help/customer/display.html?"
"nodeId=G4ZAA22U35N373NX)."
msgstr ""
"Wenn Sie in einem Teil der Welt leben, in dem der Kauf von generalüberholten "
"Laptops unüblich ist, sollten Sie sich bei eBay und Amazon umsehen. Amazon "
"bietet eine 90-tägige [Amazon Renewed Guarantee] (https://www.amazon.com/gp/"
"help/customer/display.html?nodeId=G4ZAA22U35N373NX)."

#. type: Bullet: '- '
msgid ""
"Consider buying a new laptop from vendors who guarantee the compatibility "
"with Linux and Tails like [ThinkPenguin](https://www.thinkpenguin.com/)."
msgstr ""
"Erwägen Sie den Kauf eines neuen Laptops von Anbietern, die die "
"Kompatibilität mit Linux und Tails garantieren, wie [ThinkPenguin] "
"(https://www.thinkpenguin.com/)."

#. type: Title ###
#, no-wrap
msgid "For Mac"
msgstr "Für Mac"

#. type: Plain text
msgid ""
"Unfortunately, we don't know of any Mac model that works well in Tails and "
"can run the latest macOS version."
msgstr ""
"Leider ist uns kein Mac-Modell bekannt, das in Tails gut funktioniert und "
"die neueste macOS-Version ausführen kann."

#. type: Plain text
#, no-wrap
msgid "<!--\n"
msgstr "<!--\n"

#. type: Plain text
msgid "To update the list of Mac computer:"
msgstr "So aktualisieren Sie die Liste der Mac-Computer:"

#. type: Bullet: '1. '
msgid ""
"Store an archive of WhisperBack reports from the last 6 months in a folder."
msgstr ""
"Speichern Sie ein Archiv der WhisperBack-Berichte der letzten 6 Monate in "
"einem Ordner."

#. type: Bullet: '2. '
msgid "Decrypt all the reports:"
msgstr "Entschlüsseln Sie alle Berichte:"

#. type: Plain text
#, no-wrap
msgid "   ~/Tails/blueprints/stats/whisperback_scripts/decrypt.rb\n"
msgstr "   ~/Tails/blueprints/stats/whisperback_scripts/decrypt.rb\n"

#. type: Bullet: '3. '
msgid "Extract the list of computer models:"
msgstr "Extrahieren Sie die Liste der Computermodelle:"

#. type: Plain text
#, no-wrap
msgid "   ~/Tails/blueprints/stats/whisperback_scripts/content_of.rb \"/usr/sbin/dmidecode -s system-product-name\" > machines\n"
msgstr ""
"   ~/Tails/blueprints/stats/whisperback_scripts/content_of.rb \"/usr/sbin/"
"dmidecode -s system-product-name\" > machines\n"

#. type: Bullet: '4. '
msgid "Sort and count identical models:"
msgstr "Sortieren und zählen Sie identische Modelle:"

#. type: Plain text
#, no-wrap
msgid "   grep -v Bug_report machines | sort | uniq -c | sort -rhk 1 > top\n"
msgstr "   grep -v Bug_report machines | sort | uniq -c | sort -rhk 1 > top\n"

#. type: Bullet: '5. '
msgid ""
"Share WhisperBack reports number with the Foundations team and ask them to "
"evaluate hardware compatibility hints in those reports."
msgstr ""
"Geben Sie die Nummer der WhisperBack-Berichte an das Stiftungsteam weiter "
"und bitten Sie es, die Hinweise auf die Hardwarekompatibilität in diesen "
"Berichten zu bewerten."

#. type: Plain text
#, no-wrap
msgid "   While analyzing the reports corresponding from each model, FT should:\n"
msgstr ""
"   Bei der Analyse der Berichte, die von jedem Modell stammen, sollte FT:\n"

#. type: Bullet: '   - '
msgid "Check whether they were sent from the same email address, if any"
msgstr ""
"Prüfen Sie, ob sie von derselben E-Mail-Adresse gesendet wurden, falls "
"vorhanden"

#. type: Bullet: '   - '
msgid "Check whether the wlan0 interface was the same hardware device"
msgstr "Prüfen Sie, ob die Schnittstelle wlan0 das gleiche Hardware-Gerät ist"

#. type: Bullet: '   - '
msgid "Check whether the report was about hardware compatibility issues"
msgstr ""
"Prüfen Sie, ob der Bericht Probleme mit der Hardwarekompatibilität betraf"

#. type: Bullet: '6. '
msgid ""
"Check whether these Mac models still support the latest version of macOS."
msgstr ""
"Prüfen Sie, ob diese Mac-Modelle noch die neueste Version von macOS "
"unterstützen."

#. type: Plain text
msgid "To update the list of PC computers:"
msgstr "So aktualisieren Sie die Liste der PC-Computer:"

#. type: Plain text
msgid "- Check what's commonly available with refurbishing companies."
msgstr ""
"- Erkundigen Sie sich, welche Angebote bei Sanierungsfirmen üblich sind."

#. type: Plain text
msgid "- Ask tails-assembly@ for models."
msgstr "- Fragen Sie tails-assembly@ nach Modellen."

#. type: Plain text
#, no-wrap
msgid "-->\n"
msgstr "-->\n"

#, no-wrap
#~ msgid "<div class=\"note\">\n"
#~ msgstr "<div class=\"note\">\n"

#, fuzzy, no-wrap
#~| msgid "Tails requires a 64-bit <span class=\"definition\">[[!wikipedia x86-64]]</span> compatible processor: **<span class=\"definition\">[[!wikipedia IBM_PC_compatible]]</span>** and others but not <span class=\"definition\">[[!wikipedia PowerPC]]</span> nor <span class=\"definition\">[[!wikipedia ARM_architecture desc=\"ARM\"]]</span>. Mac computers are IBM PC compatible since 2006. Tails does **not** run on most tablets and phones."
#~ msgid ""
#~ "- A 64-bit <span class=\"definition\">[[!wikipedia x86-64]]</span>\n"
#~ "  compatible processor:\n"
#~ "  - <span class=\"definition\">[[!wikipedia IBM_PC_compatible]]</span> but not\n"
#~ "    <span class=\"definition\">[[!wikipedia PowerPC]]</span> nor\n"
#~ "    <span class=\"definition\">[[!wikipedia ARM_architecture desc=\"ARM\"]]</span>.\n"
#~ "  - Most Mac computers are IBM PC compatible since 2006.\n"
#~ "  - Tails does not work with Mac models that use the [[!wikipedia Apple M1]] chip.\n"
#~ "  - Tails does not work on 32-bit computers since [[Tails 3.0|news/Tails_3.0_will_require_a_64-bit_processor]] (June 2017).\n"
#~ "  - Tails does not work on most tablets and phones.\n"
#~ msgstr "Tails benötigt einen Prozessor, der auf der <span class=\"definition\">[[!wikipedia_de AMD64 desc=\"x86-64-Architektur\"]]</span> basiert. Deshalb läuft es auf den meisten gängigen **<span class=\"definition\">[[!wikipedia_de IBM-PC-kompatibler_Computer desc=\"IBM-PC-kompatiblen Computern\"]]</span>** sowie anderen, aber nicht auf <span class=\"definition\">[[!wikipedia_de PowerPC]]</span>- oder <span class=\"definition\">[[!wikipedia_de ARM-Architektur desc=\"ARM\"]]</span>-Rechnern. MAC-Computer sind seit 2006 IBM-PC-kombatibel. Tails funktioniert **nicht** auf den meisten Tablets und Handys."

#~ msgid ""
#~ "Tails works on most reasonably recent computers, say manufactured after "
#~ "2008.  Here is a detailed list of requirements:"
#~ msgstr ""
#~ "Tails läuft auf den meisten halbwegs aktuellen Computern (d.h. solche, "
#~ "die nach 2008 hergestellt wurden). Folgende Voraussetzungen muss der "
#~ "Rechner jedoch erfüllen:"

#~ msgid ""
#~ "Either **an internal or external DVD reader** or the possibility to "
#~ "**boot from a USB stick**."
#~ msgstr ""
#~ "Ein **internes oder externes DVD-Laufwerk**, oder die Möglichkeit von "
#~ "einem **USB-Stick zu starten**."
