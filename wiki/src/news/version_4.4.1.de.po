# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2023-08-24 19:55+0000\n"
"PO-Revision-Date: 2023-12-05 10:42+0000\n"
"Last-Translator: Benjamin Held <Benjamin.Held@protonmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 4.4.1 is out\"]]\n"
msgstr "[[!meta title=\"Tails 4.4.1 wurde veröffentlicht\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Mon, 23 Mar 2020 20:00:00 +0000\"]]\n"
msgstr "[[!meta date=\"Mon, 23 Mar 2020 20:00:00 +0000\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!pagetemplate template=\"news.tmpl\"]]\n"
msgstr "[[!pagetemplate template=\"news.tmpl\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
msgid ""
"This release is an emergency release to fix security vulnerabilities in *Tor "
"Browser* and *Tor*."
msgstr ""
"Diese Version ist eine Notfallversion zur Behebung von Sicherheitslücken im *"
"Tor Browser* und in *Tor*."

#. type: Plain text
msgid "You should upgrade as soon as possible."
msgstr "Ein Upgrade sollte so schnell wie möglich durchgeführt werden."

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Title ##
#, no-wrap
msgid "Included software"
msgstr "Enthaltene Software"

#. type: Bullet: '- '
msgid ""
"Update *Tor Browser* to [9.0.7](https://blog.torproject.org/new-release-tor-"
"browser-907), which prevents [[JavaScript from sometimes being enabled in "
"the *Safest* security level of Tor Browser|news/"
"javascript_sometimes_enabled_in_safest]]."
msgstr ""
"Aktualisierung des *Tor Browser* auf [9.0.7](https://blog.torproject.org/new-"
"release-tor-browser-907), in der verhindert wird, dass [[JavaScript manchmal "
"in der *sichersten* Sicherheitsstufe des Tor Browsers aktiviert wird|news/"
"javascript_sometimes_enabled_in_safest]]."

#. type: Bullet: '- '
msgid ""
"Update *Tor* to [0.4.2.7](https://blog.torproject.org/new-"
"releases-03510-0419-0427), which fixes TROVE-2020-002, a major denial-of-"
"service vulnerability that affects both Tor relays and clients. Using this "
"vulnerability, an attacker could cause Tor to consume a huge amount of CPU, "
"disrupting its operations for several seconds or minutes, and create "
"patterns that could aid in traffic analysis."
msgstr ""
"Aktualisiert *Tor* auf [0.4.2.7](https://blog.torproject.org/new-"
"releases-03510-0419-0427), welches TROVE-2020-002 behebt, eine "
"schwerwiegende Denial-of-Service-Sicherheitsanfälligkeit, die Auswirkungen "
"sowohl auf Tor-Relais als auch Clients hat. Bei Verwendung dieser "
"Sicherheitsanfälligkeit kann ein Angriff dazu führen, dass Tor eine große "
"Menge an CPU verbraucht, den Betrieb für einige Sekunden oder Minuten "
"unterbricht und Muster erstellt, die bei der Datenanalyse für den Angreifer "
"hilfreich sein können."

#. type: Plain text
msgid ""
"- Update *Thunderbird* to [68.6.0](https://www.thunderbird.net/en-US/"
"thunderbird/68.6.0/releasenotes/)."
msgstr ""
"- Aktualisiert *Thunderbird* auf [68.6.0](https://www.thunderbird.net/en-US/"
"thunderbird/68.6.0/releasenotes/)."

#. type: Plain text
#, no-wrap
msgid "<a id=\"known-issues\"></a>\n"
msgstr "<a id=\"known-issues\"></a>\n"

#. type: Title #
#, no-wrap
msgid "Known issues"
msgstr "Bekannte Probleme"

#. type: Plain text
msgid "None specific to this release."
msgstr "Keine spezifischen Angaben zu dieser Version."

#. type: Plain text
msgid "See the list of [[long-standing issues|support/known_issues]]."
msgstr "Siehe die Liste der [[Altlasten|support/known_issues]]."

#. type: Title #
#, no-wrap
msgid "Get Tails 4.4.1"
msgstr "Tails in Version 4.4.1"

#. type: Title ##
#, no-wrap
msgid "To upgrade your Tails USB stick and keep your persistent storage"
msgstr ""
"So aktualisieren Sie Ihren Tails USB-Stick und behalten Ihren dauerhaften "
"Speicher"

#. type: Plain text
msgid ""
"- Automatic upgrades are available from 4.2, 4.2.2, 4.3, and 4.4 to 4.4.1."
msgstr ""
"- Automatische Upgrades sind von 4.2, 4.2.2, 4.3 und 4.4 zu 4.4.1 verfügbar."

#. type: Bullet: '- '
msgid ""
"If you cannot do an automatic upgrade or if Tails fails to start after an "
"automatic upgrade, please try to do a [[manual upgrade|doc/upgrade/#manual]]."
msgstr ""
"Wenn Sie kein automatisches Upgrade durchführen können oder wenn Tails nach "
"einem automatischen Upgrade nicht startet, versuchen Sie bitte ein [["
"manuelles Upgrade|doc/upgrade/#manual]]."

#. type: Title ##
#, no-wrap
msgid "To install Tails on a new USB stick"
msgstr "So installieren Sie Tails auf einem neuen USB-Stick"

#. type: Plain text
msgid "Follow our installation instructions:"
msgstr "Folgen Sie unserer Installationsanleitung:"

#. type: Bullet: '  - '
msgid "[[Install from Windows|install/windows]]"
msgstr "[[Installieren von Windows|install/windows]]"

#. type: Bullet: '  - '
msgid "[[Install from macOS|install/mac]]"
msgstr "[[Installieren von macOS|install/mac]]"

#. type: Bullet: '  - '
msgid "[[Install from Linux|install/linux]]"
msgstr "[[Installieren von Linux|install/linux]]"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\"><p>All the data on this USB stick will be lost.</p></div>\n"
msgstr ""
"<div class=\"caution\"><p>Alle Daten auf dem USB Stick werden beim "
"Installieren von Tails überschrieben.</p></div>\n"

#. type: Title ##
#, no-wrap
msgid "To download only"
msgstr "Nur zum Herunterladen"

#. type: Plain text
msgid ""
"If you don't need installation or upgrade instructions, you can download "
"Tails 4.4.1 directly:"
msgstr ""

#. type: Bullet: '  - '
msgid "[[For USB sticks (USB image)|install/download]]"
msgstr "[[Für USB-Sticks (USB-Image)|install/download]]"

#. type: Bullet: '  - '
msgid "[[For DVDs and virtual machines (ISO image)|install/download-iso]]"
msgstr "[[Für DVDs und virtuelle Maschinen (ISO-Image)|install/download-iso]]"

#. type: Title #
#, no-wrap
msgid "What's coming up?"
msgstr "Was kommt als Nächstes?"

#. type: Plain text
msgid "Tails 4.5 is [[scheduled|contribute/calendar]] for April 7."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"We need your help and there are many ways to [[contribute to\n"
"Tails|contribute]] (<a href=\"https://tails.net/donate/?r=4.4.1\">donating</a> is only one of\n"
"them). Come [[talk to us|about/contact#tails-dev]]!\n"
msgstr ""

#~ msgid ""
#~ "Have a look at our [[!tails_roadmap]] to see where we are heading to."
#~ msgstr ""
#~ "Werfen Sie einen Blick auf die [[!tails_roadmap desc=\"Roadmap\"]], um zu "
#~ "sehen, was wir als Nächstes vorhaben."
